const koa = require('koa');
const app = koa();
const router = require('koa-router')();
const parser = require('co-body');
const sendgrid = require('sendgrid');
const cors = require('koa-cors');

// try{
//   require('dotenv').config()
// }
// catch (err){
//   console.log('dotenv err', err);
// }


const FROMEMAIL = process.env.FROMEMAIL;
const APIKEY = process.env.SENDGRIDAPI;

router.get('/', function *(next) {
  this.body = 'Hello from router! '
});

router.post('/email', function *(next){
  const body = yield parser.json(this);
  console.log(body);
  const sgResponse = yield sendEmail(body);
  this.status = 200;
  this.body = 'Ok'
})


app
  .use(cors())
  .use(router.routes())
  .use(router.allowedMethods());

// app.use(function *(){
//   this.body = 'Hello World';
// });

function sendEmail({email, name, subject, message}) {
  const helper = sendgrid.mail;
  const from_email = new helper.Email(FROMEMAIL);
  const to_email = new helper.Email(FROMEMAIL);
  const content = new helper.Content('text/plain', message);
  const mail = new helper.Mail(from_email, subject, to_email, content);

  const sg = sendgrid(APIKEY);
  const request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON(),
  });
   
  return sg.API(request);
}

app.listen(3000);