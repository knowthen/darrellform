# README.md
use zeit.co => create an account
install `npm install now -g`

# Secrets
now secrets add FROMEMAIL <emailhere>
now secrets add SENDGRIDAPI <apihere>

now -e SENDGRIDAPI=@sendgridapi -e FROMEMAIL=@fromemail